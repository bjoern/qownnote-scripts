import QtQml 2.0
import com.qownnotes.noteapi 1.0

/**
 * This script creates a menu item and a button to create or jump to the current weeks workreport
 */
QtObject {
    property string headlinePrefix;
    property string defaultFolder;
    property string defaultTags;
    property bool timeInNoteName;
    property string scriptDirPath;
    property bool debug: false;

    // register your settings variables so the user can set them in the script settings
    property variant settingsVariables: [
        {
            "identifier": "defaultFolder",
            "name": "Default folder",
            "description": "The default folder where the daily notes should be stored",
            "type": "string",
            "default": "",
        },
    ];

    /**
     * Initializes the custom action
     */
    function init() {
        if (debug === true) script.log("Debug mode enabled");
        script.registerCustomAction("daily", "Create or open daily notes file", "Daily Notes", scriptDirPath + "/daily.png");
    }


    /**
     * This function is invoked when a custom action is triggered
     * in the menu or via button
     *
     * @param identifier string the identifier defined in registerCustomAction
     */
    function customActionInvoked(identifier) {
        if (identifier != "daily") {
            return;
        }

        // get the date headline
        var headline = "Daily Notes"

        //var m = new Date();
        //headline = headline + " " + ("0" + m.getHours()).slice(-2) + "." + ("0" + m.getMinutes()).slice(-2);

        var fileName = headline + ".md";

        // Check if we already have a meeting note for today.

        // When a default folder is set, make sure to search in that folder.
        // This has the highest chance of finding an existing meeting note.
        // Right now we can not search the whole database for a note with this
        // name / filename.
        if (defaultFolder && defaultFolder !== '') {
            script.jumpToNoteSubFolder(defaultFolder);
        }

	if (debug === true) script.log("Looking for file: " + fileName);
        var note = script.fetchNoteByFileName(fileName);

        // check if note was found
        if (note.id > 0) {
            // jump to the note if it was found
            if (debug === true)  script.log("found daily notes files");
            script.setCurrentNote(note);
        } else {
            // create a new meeting note if it wasn't found
            // keep in mind that the note will not be created instantly on the disk
            if (debug === true) script.log("creating new daily notes files");

            // Default folder.
            if (defaultFolder && defaultFolder !== '') {
                var msg = 'Jump to default folder \'' + defaultFolder + '\' before creating a daily notes file.';
                if (debug === true) script.log('Attempt: ' + msg);
                var jump = script.jumpToNoteSubFolder(defaultFolder);
                if (jump) {
                    if (debug === true) script.log('Success: ' + msg);
                } else {
                    if (debug === true) script.log('Failed: ' + msg);
                }
            }

            // Create the new meeting note.
            script.createNote(headline + "\n" + "=".repeat(headline.length)  + "\n\n");
        }
    }
}
