import QtQml 2.0

/**
 * This is an example for custom styling of html in the note preview
 */
QtObject {

    /**
     * This function is called when the markdown html of a note is generated
     *
     * It allows you to modify this html
     * This is for example called before by the note preview
     *
     * @param {Note} note - the note object
     * @param {string} html - the html that is about to being rendered
     * @param {string} forExport - true if the html is used for an export, false for the preview
     * @return {string} the modified html or an empty string if nothing should be modified
     */
    function noteToMarkdownHtmlHook(note, html, forExport) {
        // see http://doc.qt.io/qt-5/richtext-html-subset.html for a list of
        // supported css styles
        if (forExport) {
            var stylesheet = "h1 { color: #0082c9;} h2 { color: #0082c9;} h3 { color: #0082c9;} h4 { color: #0082c9} li.noBullet { list-style-type: none;  margin-left: -15px; }";
            html = html.split("<li>[ ]").join("<li class=\"noBullet\">&#9744;");
            html = html.split("<li>[x]").join("<li class=\"noBullet\">&#9745;");
            html = html.split("<li>[X]").join("<li class=\"noBullet\">&#9745;");
        } 
        html = html.replace("</style>", stylesheet + "</style>");
        return html;
    }
}

