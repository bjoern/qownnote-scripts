import QtQml 2.0
import com.qownnotes.noteapi 1.0

/**
 * This script creates a menu item and a button to create or jump to the current weeks workreport
 */
QtObject {
    property string headlinePrefix;
    property string defaultFolder;
    property string defaultTags;
    property bool timeInNoteName;
    property string scriptDirPath;
    property bool debug: false;

    // register your settings variables so the user can set them in the script settings
    property variant settingsVariables: [
        {
            "identifier": "headlinePrefix",
            "name": "Headline prefix",
            "description": "Please enter a prefix for your workreport headline:",
            "type": "string",
            "default": "Workreport",
        },
        {
            "identifier": "defaultFolder",
            "name": "Default folder",
            "description": "The default folder where the newly created workreport should be placed. Specify the path to the folder relative to the note folder. Make sure that the full path exists. Examples: to place new workreport in the subfolder 'Workreports' enter: \"Workreports\"; to place new workreport in the subfolder 'Reports' in the subfolder 'Work' enter: \"Work/Reports\". Leave blank to disable (notes will be created in the currently active folder).",
            "type": "string",
            "default": "",
        },
        {
            "identifier": "defaultTags",
            "name": "Default tags",
            "description": "One or more default tags (separated by commas) to assign to a newly created workreport. Leave blank to disable auto-tagging.",
            "type": "string",
            "default": "meeting",
        },
        {
            "identifier": "timeInNoteName",
            "name": "Time in workreport",
            "description": "Add time (HH:mm) in 'Workreport' note name.",
            "type": "boolean",
            "default": false,
        },
    ];

    /**
     * Initializes the custom action
     */
    function init() {
        if (debug === true) script.log("Debug mode enabled");
        script.registerCustomAction("workreport", "Create or open a Workreport", "Workreport", scriptDirPath + "/checklist.png");
    }

    function getWeek() {
    	     var date = new Date();
	     date.setHours(0, 0, 0, 0);
	     date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
	     var week1 = new Date(date.getFullYear(), 0, 4);
	     return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
    }


    /**
     * This function is invoked when a custom action is triggered
     * in the menu or via button
     *
     * @param identifier string the identifier defined in registerCustomAction
     */
    function customActionInvoked(identifier) {
        if (identifier != "workreport") {
            return;
        }

        // get the date headline
        var m = new Date();
        var headline = headlinePrefix + " CW " + getWeek();

        if (timeInNoteName) {
          headline = headline + " " + ("0" + m.getHours()).slice(-2) + "." + ("0" + m.getMinutes()).slice(-2);
        }

        var fileName = headline + ".md";

        // Check if we already have a meeting note for today.

        // When a default folder is set, make sure to search in that folder.
        // This has the highest chance of finding an existing meeting note.
        // Right now we can not search the whole database for a note with this
        // name / filename.
        if (defaultFolder && defaultFolder !== '') {
            script.jumpToNoteSubFolder(defaultFolder);
        }

	if (debug === true) script.log("Looking for file: " + fileName);
        var note = script.fetchNoteByFileName(fileName);

        // check if note was found
        if (note.id > 0) {
            // jump to the note if it was found
            if (debug === true)  script.log("found meeting note: " + headline);
            script.setCurrentNote(note);
        } else {
            // create a new meeting note if it wasn't found
            // keep in mind that the note will not be created instantly on the disk
            if (debug === true) script.log("creating new workreport: " + headline);

            // Default folder.
            if (defaultFolder && defaultFolder !== '') {
                var msg = 'Jump to default folder \'' + defaultFolder + '\' before creating a new workreport.';
                if (debug === true) script.log('Attempt: ' + msg);
                var jump = script.jumpToNoteSubFolder(defaultFolder);
                if (jump) {
                    if (debug === true) script.log('Success: ' + msg);
                } else {
                    if (debug === true) script.log('Failed: ' + msg);
                }
            }

            // Create the new meeting note.
            script.createNote(headline + "\n" + "=".repeat(headline.length)  + "\n\n## Attention/Urgent\n\n\n\n## What I did last week\n\n\n\n## Next week\n\n\n\n# Meeting notes\n\n## Professional Service Call\n\n\n\n## Sales Call\n\n\n\n## Company Call\n\n\n\n# ToDo's of the Week\n\n\n\n");

            // Default tags.
            if (defaultTags && defaultTags !== '') {
                defaultTags
                    // Split on 0..* ws, 1..* commas, 0..* ws.
                    .split(/\s*,+\s*/)
                    .forEach(function(i) {
                        if (debug === true) script.log('Tag the new meeting note with default tag: ' + i);
                        script.tagCurrentNote(i);
                    });
            }
        }
    }
}
