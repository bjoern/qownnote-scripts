import QtQml 2.0
import QtQuick 2.0
import com.qownnotes.noteapi 1.0

/**
 * This script creates the initial files for a projects folder
 */
QtObject {
    property string scriptDirPath;
    property bool debug: false;
    property bool hasOverview;
    property string projectsRoot;

    property variant settingsVariables: [
        {
            "identifier": "hasOverview",
            "name": "Jump to Prject Overview on startup",
            "description": "if a Overview document (overview.md) exists in the main projects folder the script can automatically jump to it on startup",
            "type": "boolean",
            "default": "false",
        },
        {
            "identifier": "projectsRoot",
            "name": "Projects root folder",
            "description": "Root folder which contain all the projects",
            "type": "string",
            "default": "nextcloud/Projects",
        },
    ];


    /**
     * Initializes the custom action
     */
    function init() {
        if (debug === true) script.log("Debug mode enabled");
        script.registerCustomAction("projectFolder", "Create initial files for project folders", "Init Project Folder", scriptDirPath + "/project.png");
        if (hasOverview) {
            script.registerCustomAction("runningProjects", "Running projects", "Show running projects", scriptDirPath + "/running.svg");
        }
    }

    /**
     * This function is invoked when a custom action is triggered
     * in the menu or via button
     *
     * @param identifier string the identifier defined in registerCustomAction
     */
    function customActionInvoked(identifier) {
        if (identifier == "projectFolder") {
            initProjectFolder();
        } else if (identifier == "runningProjects") {
            showRunningProjects();
        }

    }

    /**
     *  setup new project folder
     */
    function initProjectFolder() {
        // create "next steps" file
        var nextStepsFileName = "Next Steps.md";
        file = script.fetchNoteByFileName(nextStepsFileName);

        // check if file was found
        if (file.id == 0) {
            if (debug === true) script.log("creating new Next Steps file");
            script.createNote("Next Steps\n" + "==========\n\n\n");
        } else {
            if (debug === true) script.log("Next Steps file already exists, ID: " + file.id);
        }
        
        // create overview file
        var overviewFileName = "Overview.md";
        var file = script.fetchNoteByFileName(overviewFileName);

        // check if file was found
        if (file.id == 0) {
            if (debug === true) script.log("creating new Overview file");
            // Create the new overview file.
            script.createNote("Overview\n" + "========\n\nAccount Manager: \n\nContact: \n\n## Summary\n\n## References\n\n### Professional Service Tickets\n\n## Progress\n\n\n");
        } else {
            if (debug === true) script.log("Overview file already exists, ID: " + file.id);
        }
        
        // create meeting notes folder
        var note = script.currentNote();
        var fileName = note.fullNoteFilePath;
        script.log("create meeting notes folder for: " + fileName);
        mainWindow.createNewNoteSubFolder("meeting notes");
    }

    /**
     *  go to overview page with all runing projects. Runing projects are defined as projects wich contain some "next steps"
     */
    function showRunningProjects() {
        script.jumpToNoteSubFolder(projectsRoot);
	var note = script.fetchNoteByFileName("Overview.md");
	if (note.id > 0) {
            // jump to the note if it was found
            if (debug === true)  script.log("Jump to overview page!");
            script.setCurrentNote(note);
	} else {
	    script.log("No files with running projects");
	}
    }


    /**
     * We need a timer to delay the jump to the note until QOwnNotes is setup
     * completely
     */
    property QtObject timer: Timer {
        interval: 2000
        repeat: false
        running: true

        onTriggered: {
                if (!hasOverview) return;
		script.jumpToNoteSubFolder(projectsRoot);
		var note = script.fetchNoteByFileName("Overview.md");
		if (note.id > 0) {
			// jump to the note if it was found
			if (debug === true)  script.log("Jump to overview page!");
			 script.setCurrentNote(note);
		}
        }
    }

}
